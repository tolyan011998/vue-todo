import firebase from 'firebase/app'

export default {
  state: {
    todo: {}
  },
  mutations: {
    setTodo (state, a) {
      for (let i in a) {
        if (i !== 'info') {
          state.todo[i] = a[i]
        }
      }
    }
  },
  actions: {
    async fetchTodo ({ dispatch, commit }) {
      try {
        const uid = await dispatch('getuid')
        const a = (await firebase.database().ref(`Users/${uid}`).once('value')).val()
        commit('setTodo', a)
      } catch (e) {

      }
    },
    async addTodoList ({ dispatch }, { i, title, color }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/todo_${i}`).set({
          title,
          color
        })
        alert('Спиосок дел ' + title + ' добавлен')
        await dispatch('fetchTodo')
      } catch (e) {
        console.log('Дело не добавлено, ошибка')
      }
    },
    async removeTodoList ({ dispatch }, { i }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${i}`).remove()
        await dispatch('fetchTodo')
      } catch (e) {
        console.log(e)
      }
    },
    async editTodoList ({ dispatch }, { i, title }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${i}`).update({
          title
        })
        alert('Спиосок дел ' + title + ' изменен')
        await dispatch('fetchTodo')
      } catch (e) {
        console.log('Дело не добавлено, ошибка')
      }
    },
    async changeColor ({ dispatch }, { keyTodo, color }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${keyTodo}`).update({
          color
        })
      } catch (e) {
        console.log(e)
      }
    },
    async addList ({ dispatch }, { key, i, title, urgently, check, date }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${key}/list_${i}`).set({
          title,
          urgently,
          check,
          date
        })
        const todoList = (await firebase.database().ref(`Users/${uid}/${key}`).once('value')).val()
        alert('Дело ' + title + 'добавлено в ' + todoList.title)
        await dispatch('fetchTodo')
      } catch (e) {
        console.log('Дело не добавлено, ошибка')
      }
    },
    async removeList ({ dispatch }, { keyTodo, keyList }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${keyTodo}/${keyList}`).remove()
        await dispatch('fetchTodo')
      } catch (e) {
        console.log(e)
      }
    },
    async editList ({ dispatch }, { keyTodo, keyList, title, urgently }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${keyTodo}/${keyList}`).update({
          title,
          urgently
        })
        console.log(await firebase.database().ref(`Users/${uid}/${keyTodo}/${keyList}`))
        await dispatch('fetchTodo')
      } catch (e) {
        console.log('Дело не добавлено, ошибка')
      }
    },
    async checkList ({ dispatch }, { keyTodo, keyList, check }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${keyTodo}/${keyList}`).update({
          check
        })
        await dispatch('fetchTodo')
      } catch (e) {
        console.log(e)
      }
    },
    async colorChange ({ dispatch }, { keyTodo, color }) {
      try {
        const uid = await dispatch('getuid')
        await firebase.database().ref(`Users/${uid}/${keyTodo}`).update({
          color
        })
        await dispatch('fetchTodo')
      } catch (e) {
        console.log(e)
      }
    }
  },
  getters: {
    todo: s => s.todo
  }
}
