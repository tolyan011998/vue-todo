import firebase from 'firebase/app'

export default {
  state: {
    todo: {},
    list: {}
  },
  mutations: {
    setTodo (state, todo) {
      for (let i in todo) {
        if (i !== 'info') {
          state.todo[i] = todo[i]
          // console.log(state.todo[i])
          for (let j in todo) {
            if (j !== 'title') {
              state.list[j] = state.todo[i]
              // console.log(state.list[j])
            }
          }
        }
      }
    }
    // setList (state, todo, list) {
    //   for (let j in todo) {
    //     if (j !== 'title') {
    //       state.list[j] = todo[j]
    //       console.log(state.list[j])
    //     }
    //   }
    // }
  },
  actions: {
    async fetchList ({ dispatch, commit }) {
      try {
        const uid = await dispatch('getuid')
        const todo = (await firebase.database().ref(`/Users/${uid}`).once('value')).val()
        commit('setTodo', todo)
      } catch (e) {

      }
    }
  },
  getters: {

  }
}
