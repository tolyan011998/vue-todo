import Vue from 'vue'
import Vuelidate from 'vuelidate'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

window.toastr = require('toastr')

Vue.use(VueToastr2)
Vue.use(Vuelidate)
Vue.use(BootstrapVue)

const firebaseConfig = {
  apiKey: 'AIzaSyBz_Dtq3Myy5d32YJ0kwC8-qGTAHaux1O4',
  authDomain: 'vue-todo-tolyan.firebaseapp.com',
  databaseURL: 'https://vue-todo-tolyan.firebaseio.com',
  projectId: 'vue-todo-tolyan',
  storageBucket: 'vue-todo-tolyan.appspot.com',
  messagingSenderId: '593194615878',
  appId: '1:593194615878:web:1f3518c4a2495bba3ff647',
  measurementId: 'G-LHX7YHMHQF'
}

firebase.initializeApp(firebaseConfig)

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
